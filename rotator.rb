class Rotator
  attr_accessor(:cnt, :step)

  def initialize(step)
    @cnt = 0
    if (step)
      @step = step
    else
      @step = 0
    end
  end

  def get()
    r = ""
    if (0 == @cnt % 4)
      r = "|"
    elsif (1 == @cnt % 4)
      r = "/"
    elsif (2 == @cnt % 4)
      r = "-"
    elsif (3 == @cnt % 4)
      r = "\\"
    end
    if (0 < @cnt)
      if (0 == (@cnt % @step))
        r = "\b.."
      else
        r = "\b" + r
      end
    end

    @cnt += 1
    return r
  end

  def reset()
    @cnt = 0
  end
end
