#!/usr/bin/env ruby

class String
  def to_range()
    toks = self.split(/\s*\.\.\s*/)
    return (toks[0].to_i .. toks[1].to_i)
  end
end

class Rect
  attr_accessor(:offsetX, :offsetY, :width, :height)
  attr_accessor(:data)

  def x1
    return @offsetX
  end

  def y1
    return @offsetY
  end

  def x1=(v)
    @offsetX = v
    return self
  end

  def y1=(v)
    @offsetY = v
    return self
  end

  def w
    return @width
  end

  def h
    return @height
  end

  def w=(v)
    @width = v
    return self
  end

  def h=(v)
    @height = v
    return self
  end

  def x2
    return @offsetX + @width
  end

  def y2
    return @offsetY + @height
  end

  def x2=(v)
    @width = v - @offsetX
    return self
  end

  def y2=(v)
    @height = v - @offsetY
    return self
  end

  def averageX
    return @offsetX + (0.5 * @width)
  end

  def averageY
    return @offsetY + (0.5 * @height)
  end

  def avgX
    return self.averageX
  end

  def avgY
    return self.averageY
  end

  def sizeOfArea
    return @width * @height
  end

  def setByX1Y1X2Y2(v1, v2, v3, v4)
    # return self.x1=(v1).y1=(v2).x2=(v3).y2=(v4)
    @offsetX = v1
    @offsetY = v2
    @width   = v3 - v1
    @height  = v4 - v2
    return self
  end

  def setByX1Y1X2Y2Arr(arr)
    return self.setByX1Y1X2Y2(arr[0], arr[1], arr[2], arr[3])
  end

  def setByX1X2Y1Y2(v1, v2, v3, v4)
    return self.setByX1Y1X2Y2(v1, v3, v2, v4)
  end

  def setByRanges(xRange, yRange)
    return self.setByX1X2Y1Y2(xRange.first, xRange.last, yRange.first, yRange.last)
  end

  def setByRangeStrs(s1, s2)
    return self.setByRanges(s1.to_range(), s2.to_range())
  end

  def setByXYWH(v1, v2, v3, v4)
    @offsetX = v1
    @offsetY = v2
    @width   = v3
    @height  = v4
    return self
  end

  def setByXYWHArr(arr)
    return self.setByXYWH(arr[0], arr[1], arr[2], arr[3])
  end

  def setByCenterXYAndWH(x, y, w, h)
    @offsetX = x - (0.5) * w
    @offsetY = y - (0.5) * h
    @width = w
    @height = h
    return self
  end

  def from_s(s)
    toks = s.split("_")
    @width, @height, @offsetX, @offsetY = toks.pop.split(/[x\+]/).collect{|t| t.to_f}
    @data = Hash.new if (@data == nil)
    if (toks.last =~ /^rot/)
      @data["rot"] = toks.pop.gsub(/^rot/, "").to_f
    end
    @data["basename"] = toks.join("_")
    return self
  end

  def from_s_including_basename_and_cropindex(s, suffix)
    suffix = suffix.gsub(/^\.*/, "") if (suffix)

    @data = Hash.new

    toks = s.split("_")
    @width, @height, @offsetX, @offsetY = toks.pop.split(/[x\+]/).collect{|t| t.to_f}

    if (toks.last =~ /^rot/)
      @data["rot"] = toks.pop.gsub(/^rot/, "").to_f
    end

    if (toks.last =~ /^[0-9]+$/)
      @data["cropIndex"] = toks.pop.to_i
    end

    @data["basename"] = toks.join("_")
    if (suffix && suffix.length > 0)
      @data["basename"] = @data["basename"] + "." + suffix
    end

    return self
  end

  def basename()
    return nil if (@data == nil || !@data.include?("basename"))
    return @data["basename"]
  end

  def from_hs(hs)
    @data = hs
    @width = hs["width"].to_f
    @height = hs["height"].to_f
    @offsetX = hs["offsetX"].to_f
    @offsetY = hs["offsetY"].to_f
    @rot = hs["rot"].to_f if (hs.include?("rot"))
    ["width", "height", "offsetX", "offsetY", "rot"].each{|k| @data.delete(k)}
    return self
  end

  def to_s
    s = sprintf("%04dx%04d+%04d+%04d", @width, @height, @offsetX, @offsetY)
    if (@data != nil && @data["rot"] != nil)
      s = sprintf("rot%s%4.2f_", (@data["rot"] < 0 ? "-" : "+"), @data["rot"].abs) + s
    end
    return s
  end

  def to_clsnm
    return sprintf("w%d-h%d-x%d-y%d", @width, @height, @offsetX, @offsetY)
  end


  def to_json_hash
    hs = @data.clone
    hs["offsetX"] = @offsetX
    hs["offsetY"] = @offsetY
    hs["width"]   = @width
    hs["height"]  = @height
    hs["rot"]  = @rot if (@rot != nil && @rot != 0)
    return hs
  end

  def to_json_hash_i
    hs = @data.clone
    hs["offsetX"] = @offsetX.to_i
    hs["offsetY"] = @offsetY.to_i
    hs["width"]   = @width.to_i
    hs["height"]  = @height.to_i
    hs["rot"]  = @rot if (@rot != nil && @rot != 0)
    return hs
  end

  def setByLibXmlElement(e)
    @offsetX = e.attributes["x"].to_f
    @offsetY = e.attributes["y"].to_f
    @width   = e.attributes["width"].to_f
    @height  = e.attributes["height"].to_f
    return self
  end

  def setByNokogiriElement(e)
    @offsetX = e["x"].to_f
    @offsetY = e["y"].to_f
    @width   = e["width"].to_f
    @height  = e["height"].to_f
    return self
  end


  def copyToXmlAttrHash(e)
    e.attributes["x"] = @offsetX.to_s
    e.attributes["y"] = @offsetY.to_s
    e.attributes["width"] = @width.to_s
    e.attributes["height"] = @height.to_s
    (@data.keys - ["x", "y", "width", "height"]).each do |k|
      e.attributes[k] = @data[k].to_s
    end
    return e
  end

  def to_libxml_node
    return self.copyToXmlAttrHash(LibXML::XML::Node.new("rect"))
  end

  def getQuantized
    qcr = Rect.new.setByXYWH(@offsetX.to_i, @offsetY.to_i, @width.to_i, @height.to_i)
    qcr.data = @data.clone
    return qcr
  end

  def hasOverlapXWith(another)
    if ((self.w + another.w) < [self.x2, another.x2].max - [self.x1, another.x1].min)
      return false
    else
      return true
    end
  end

  def hasOverlapYWith(another)
    if ((self.h + another.h) < [self.y2, another.y2].max - [self.y1, another.y1].min)
      return false
    else
      return true
    end
  end

  def getOverlapXRange(another)
    if (self.hasOverlapXWith(another))
      xs = [self.x1, self.x2, another.x1, another.x2].sort
      return (xs[1]..xs[2])
    else
      return nil
    end
  end

  def getOverlapYRange(another)
    if (self.hasOverlapYWith(another))
      ys = [self.y1, self.y2, another.y1, another.y2].sort
      return (ys[1]..ys[2])
    else
      return nil
    end
  end

  def hasOverlapWith(another)
    if (self.hasOverlapXWith(another) && self.hasOverlapYWith(another))
      return true
    else
      return false
    end
  end

  def getOverlapWith(another)
    if (self.hasOverlapWith(another))
      xs = [self.x1, self.x2, another.x1, another.x2].sort
      ys = [self.y1, self.y2, another.y1, another.y2].sort
      return Rect.new.setByX1Y1X2Y2(xs[1], ys[1], xs[2], ys[2])
    end
    return nil
  end

  def getRangeX
    return (self.x1)..(self.x2)
  end

  def getRangeY
    return (self.y1)..(self.y2)
  end

  def includesX(x)
    return self.getRangeX.include?(x)
  end

  def includesY(y)
    return self.getRangeY.include?(y)
  end
  def coversXOf(another)
    xr = self.getRangeX
    if (!xr.include?(another.x1))
      return false
    elsif (!xr.include?(another.x2))
      return false
    else
      return true
    end
  end

  def coversYOf(another)
    yr = self.getRangeY
    if (!yr.include?(another.y1))
      return false
    elsif (!yr.include?(another.y2))
      return false
    else
      return true
    end
  end

  def covers(another)
    if (!self.coversXOf(another))
      return false
    elsif (!self.coversYOf(another))
      return false
    else
      return true
    end
  end

  def includesCenterOf(another)
    if (self.includesX(another.averageX) && self.includesY(another.averageY))
      return true
    else
      return false
    end
  end

  def getSupersetWith(another)
    return Rect.new.setByX1Y1X2Y2( [self.x1, another.x1].min,
                                   [self.y1, another.y1].min,
                                   [self.x2, another.x2].max,
                                   [self.y2, another.y2].max )
  end

  def extendToCover(another)
    return self.setByX1Y1X2Y2( [self.x1, another.x1].min,
                               [self.y1, another.y1].min,
                               [self.x2, another.x2].max,
                               [self.y2, another.y2].max )
  end

  def distant2From(another)
    r2  = ( self.averageX - another.averageX ) ** 2
    r2 += ( self.averageY - another.averageY ) ** 2
    return r2
  end

  def distantFrom(another)
    return Math::sqrt(self.distant2From(another))
  end

  def makeRectForRot(rN)
    rOld = self.data["rot"] * Math::PI / 180.0
    rNew = rN * Math::PI / 180.0

    l0 = Math::sqrt(self.x1**2 + self.y1**2)
    r0 = Math::atan2(y1, x1) + rOld
    x0 = l0 * Math::cos(r0 - rNew)
    y0 = l0 * Math::sin(r0 - rNew)

    l1 = Math::sqrt(self.x2**2 + self.y1**2)
    r1 = Math::atan2(y1, x2) + rOld
    x1 = l0 * Math::cos(r1 - rNew)
    y1 = l0 * Math::sin(r1 - rNew)

    l2 = Math::sqrt(self.x1**2 + self.y2**2)
    r2 = Math::atan2(y2, x1) + rOld
    x2 = l2 * Math::cos(r2 - rNew)
    y2 = l2 * Math::sin(r2 - rNew)

    l3 = Math::sqrt(self.x2**2 + self.y2**2)
    r3 = Math::atan2(y2, x2) + rOld
    x3 = l3 * Math::cos(r3 - rNew)
    y3 = l3 * Math::sin(r3 - rNew)

    xs = [x0, x1, x2, x3].sort
    ys = [y0, y1, y2, y3].sort

    r = Rect.new.setByX1Y1X2Y2(xs.first.ceil, ys.first.ceil, xs.last.floor, ys.last.floor)
    r.data = self.data.clone
    r.data["rot"] = rN
    return r
  end

  def resize(f)
    r = Rect.new.setByXYWH(@offsetX * f, @offsetY * f, @width * f, @height * f)
    r.data = self.data.clone if (data != nil)
    return r
  end

  def resize!(f)
    @offsetX = @offsetX * f
    @offsetY = @offsetY * f
    @width   = @width * f
    @height  = @height * f
    return self
  end

  def setData(k, v)
    @data = Hash.new if (@data == nil)
    @data[k] = v
    return self
  end

  def hasRot()
    return @data.include?("rot")
  end

  def getRot()
    return @data["rot"] if (self.hasRot())
    return 0
  end

  def getRad()
    return self.getRot() * Math::PI / 180
  end

  def setByCornersAsComplex(args)
    xs = args.collect{|cx| cx.real}
    ys = args.collect{|cx| cx.imaginary}
    return self.setByX1Y1X2Y2(xs.min, ys.min, xs.max, ys.max)
  end

  def getCornersAsComplex()
    ll_cx = Complex.rect(self.x1, self.y1)
    lr_cx = Complex.rect(self.x2, self.y1)
    ul_cx = Complex.rect(self.x1, self.y2)
    ur_cx = Complex.rect(self.x2, self.y2)
    return [ll_cx, lr_cx, ul_cx, ur_cx]
  end

  def getUnrotatedCornersAsComplex()
    return self.getCornersAsComplex().collect{|cx| Complex.polar(cx.abs, cx.angle - self.getRad())}
  end

  def getUnrotatedCorners()
    return self.getUnrotatedCornersAsComplex().collect{|cx| cx.rect}
  end

  def getOffsetAsComplex()
    return Complex.rect(self.offsetX, self.offsetY)
  end

  def getSizeAsComplex()
    return Complex.rect(self.width, self.height)
  end

  def getImgSide()
    if (@data == nil || !@data.include?("baseWidth"))
      return nil
    elsif (self.averageX * 2 < data["baseWidth"].to_i )
      return "left" 
    else
      return "right" 
    end
  end

  def getLeafSide()
    if (@data == nil || !@data.include?("spreadOrLeaf"))
      return nil
    elsif (@data["spreadOrLeaf"] == "spread") 
      return (self.getImgSide() == "left" ? "right" : "left")
    elsif (@data["spreadOrLeaf"] == "leaf")
      return self.getImgSide()
    else
      return nil
    end
  end

  def getSpreadSide()
    if (@data == nil || !@data.include?("spreadOrLeaf"))
      return nil
    elsif (@data["spreadOrLeaf"] == "spread") 
      return ("spread" + self.getImgSide())
    elsif (@data["spreadOrLeaf"] == "leaf")
      return ("spread" + (self.getImgSide() == "left" ? "right" : "left"))
    else
      return nil
    end
  end

  def getScaled(f)
    # self.resize(f)
    r = Rect.new.setByXYWH(self.x1 * f, self.y1 * f, self.w * f, self.h * f)
    r.data = self.data.clone if (self.data != nil)
    return r
  end

  def to_iiif_region
    # TODO: the rotation feature of IIIF is "post-cropped" rotation,
    #       but the rotation of Rect class is "pre-cropping" rotation,
    #       not directly supported yet
    s = sprintf("%04d,%04d,%04d,%04d", @offsetX, @offsetY, @width, @height)
    return s
  end

end

class Hash
  def to_lined_json()
    return JSON.generate(self, {
      :object_nl => "",
      :array_nl => "\n",
      :space => " ",
      :indent => " "
    }).gsub(/\{ +/, "{ ").gsub(/ +\}/, " }").gsub(/, +/, ", ")
  end
end

class Array
  def to_lined_json()
    return JSON.generate(self, {
      :object_nl => "",
      :array_nl => "\n",
      :space => " ",
      :indent => " "
    }).gsub(/\{ +/, "{ ").gsub(/ +\}/, " }").gsub(/, +/, ", ")
  end
end
