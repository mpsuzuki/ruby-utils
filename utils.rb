#!/usr/bin/env ruby
class Object
  def isString()
    return false
  end

  def isArray()
    return false
  end

  def isHash()
    return false
  end
end

class Hash
  def isHash()
    return true
  end
end


class String
  IMG_SUFFIXES = ["bmp", "eps", "gif", "jpeg", "jpg", "pdf", "png", "ps", "svg", "svgz", "tif", "tiff"]

  def isString()
    return true
  end

  def insertBOM
    return sprintf("%c%c%c", 0xEF, 0xBB, 0xBF) + self
  end

  def dirname
    return File::dirname(self)
  end

  def basename
    return File::basename(self)
  end

  def suffix
    return self.split(".").last
  end

  def suffix_lowercase
    return self.suffix().downcase()
  end

  def has_img_suffix()
    return IMG_SUFFIXES.include?( self.split(".").last.downcase() )
  end

  def get_img_suffix(dot)
    if (self.has_img_suffix())
      return (dot + self.split(".").last)
    else
      return ""
    end
  end

  def remove_img_suffix()
    toks = self.split(".")
    suffix = toks.last.downcase
    if ( IMG_SUFFIXES.include?( suffix ) ) 
      toks.pop()
    end
    return toks.join(".")
  end

  def replace_suffix(sfx)
    toks = self.split(".")
    toks.pop()
    toks.push(sfx)
    return toks.join(".")
  end

  def get_img_pathname_as(pathname)
    d = File::dirname(pathname)
    if (d == ".")
      return self.basename.remove_img_suffix() + pathname.get_img_suffix(".")
    else
      return d + "/" + self.basename.remove_img_suffix() + pathname.get_img_suffix(".")
    end
  end

  def basename_without_img_suffix()
    return File::basename(self).remove_img_suffix()
  end

  def basename_without_suffix(len=4)
    b = File::basename(self)
    toks = b.split(".")
    if (toks.pop().length <= len)
      b = toks.join(".")
    end
    return b
  end

  def toCamelCase(opt)
    sep = "-" if (opt == nil || !opt.include?(sep))
    toks = []
    toks = self.split(sep)
    toks.each_index do |i|
      if (i == 0)
        toks[i] = toks[i].downcase()
      else
        toks[i] = toks[i][0..0].upcase() + toks[i][1..-1].downcase()
      end
    end
    return toks.join("")
  end

  def getUCS4s_all()
    return self.encode("ucs-4be").unpack("N*")
  end

  def getUCS4s()
    return [ self.getUCS4s_all().first ]
  end

  def getUCS4sHex_all()
    return self.getUCS4s_all().collect{|u| sprintf("U+%04X", u)}
  end

  def getUCS4sHex()
    return [ self.getUCS4sHex_all().first ]
  end

  def startsWithCJK()
    u = self.getUCS4s().first
    return true if (0x3400 <= u && u <= 0x9FFF)
    return true if (0xF900 <= u && u <= 0xFAFF)
    return true if (0x20000 <= u && u <= 0x40000)
    return false
  end

  def startsWithLatin()
    return self[0..0] =~ /^[A-Za-z]/
  end

  def startsWith(s)
    return (self[0...s.length] == s)
  end

  def endsWith(s)
    return (self[(-s.length)..-1] == s)
  end

  def getCJKBlock()
    u = self[0..0].encode("ucs-4be").unpack("N").first
    if ((0x3400..0x4DFF).include?(u))
      return 1 # ExtA
    elsif ((0x4E00..0x9FFF).include?(u))
      return 0 # URO
    elsif (0x20000 <= u)
      return 2 # ExtB and further
    else
      return 9 # Compatibility
    end
  end

  def getCJKSortKey()
    block = self.getCJKBlock()
    ucs = self.getUCS4s().first
    return ((block << 24) + ucs)
  end

  def isCJKIdeograph()
    str = self
    return (0...self.length).to_a().all?{|i|
      u = str[i..i].encode("ucs-4be").unpack("N").first
      if ((0x3400..0x4DFF).include?(u))
        true
      elsif ((0x4E00..0x9FFF).include?(u))
        true
      elsif (0x20000 <= u && u < 0x40000)
        true
      elsif ((0xF900..0xFAFF).include?(u))
        true
      else
        false
      end
    }
  end

  def isInt()
    return (self =~ /^[0-9]+$/)
  end

  def isFixedFloat()
    return (self =~ /^[0-9]+\.$/ || self =~ /^\.[0-9]+$/ || self =~ /^[0-9]+\.[0-9]+$/)
  end

  def noSign()
    if (self[0..0] == "-")
      return self[1..-1]
    elif (self[0..0] == "+")
      return self[1..-1]
    else
      return self
    end
  end

  def getCJKBlockName()
    nm2rng = {
      "ExtA"  => (0x3400..0x4DB5),
      "ExtA+" => (0x4DB6..0x4DBF),
      "URO"   => (0x4E00..0x9FA5),
      "URO+"  => (0x9FA6..0x9FFF),
      "CmptBMP" => (0xF900..0xFA2F),
      "ExtB"  => (0x20000..0x2A6D6),
      "ExtB+" => (0x2A6D6..0x2A6FF),
      "ExtC"  => (0x2A700..0x2B734),
      "ExtC+" => (0x2A735..0x2B73F),
      "ExtD"  => (0x2B740..0x2B81F),
      "ExtE"  => (0x2B820..0x2CEA1),
      "ExtF"  => (0x2CEB0..0x2EBE0),
      "ExtI"  => (0x2EBF0..0x2EED5),
      "CmptSIP" => (0x2F800..0x2FA1F),
      "ExtG"  => (0x30000..0x3134A),
      "ExtH"  => (0x31350..0x323AF)
    }
    u = self[0..0].encode("ucs-4be").unpack("N").first
    return nm2rng.keys().select{|nm| nm2rng[nm].include?(u)}.first
  end
end

class Array
  def isArray()
    return true
  end

  def getCommonPrefix()
    return "" if (self.first.class != String || self.collect{|m| m.class.to_s}.uniq.length > 1)

    maxLen = self.collect{|m| m.length}.min

    chars = []
    (0...maxLen).to_a.each do |i|
      chars[i] = self.collect{|m| m[i..i]}.uniq.length
    end

    i0 = chars.find_index{|l| l > 1}
    return self.first[0, i0]
  end

  def removeCommonPrefix()
    return self if (self.first.class != String || self.collect{|m| m.class.to_s}.uniq.length > 1)

    l = getCommonPrefix.length
    return self.collect{|s| s[l..-1]}
  end

  def frequency()
    hsTmp = Hash.new
    self.uniq.each do |v|
      hsTmp[v] = self.count(v)
    end

    hs = Hash.new
    hsTmp.keys.sort_by{|v| hsTmp[v]}.reverse.each do |v|
      hs[v] = hsTmp[v]
    end

    return hs
  end

  def sortByCJKBlock()
    return self.sort{|a,b|
      blk_a = a.getCJKBlock()
      blk_b = b.getCJKBlock()
      if (blk_a == blk_b)
        a[0..0] <=> b[0..0]
      else
        blk_a <=> blk_b
      end
    }
  end

  def to_rangef()
    my_min = self.min().to_f()
    my_max = self.max().to_f()
    return my_min..my_max
  end

  def to_rangei()
    my_min = self.min().to_i()
    my_max = self.max().to_i()
    return my_min..my_max
  end

  def first_last()
    return [self.first, self.last]
  end
end

class Fixnum
  def hex
    return "%x" % self
  end

  def to_hhmmss
    hh = self / 3600
    mm = (self - (hh * 3600)) / 60
    ss = self - (hh * 3600) - (mm * 60)
    sprintf("%02d:%02d:%02d", hh, mm, ss)
  end
end

class Float
  def hex
    return "%x" % self
  end

  def to_hhmmss
    hh = (self / 3600).floor
    mm = ((self - (hh * 3600)) / 60).floor
    ss = self - (hh * 3600) - (mm * 60)
    sprintf("%02d:%02d:%05.2f", hh, mm, ss)
  end
end

class Hash
  def to_lined_json()
    return JSON.generate(self, {
      :object_nl => "",
      :array_nl => "\n",
      :space => " ",
      :indent => " "
    }).gsub(/\{ +/, "{ ").gsub(/ +\}/, " }").gsub(/, +/, ", ")
  end
end

class Array
  def to_lined_json()
    return JSON.generate(self, {
      :object_nl => "",
      :array_nl => "\n",
      :space => " ",
      :indent => " "
    }).gsub(/\{ +/, "{ ").gsub(/ +\}/, " }").gsub(/, +/, ", ")
  end
end

class Range
  def diff
    return (self.last - self.first)
  end
end
