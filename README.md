ruby-utils
==========

# getOpts.rb
"Opts" class to help the parsing of ARGV.

# libRect.rb
"Rect" class to help the interchange between JSON hash and SVG element.

# utils.rb
Various extentions to Object, Hash, String, Array, Fixnum, Float, and Range.

# nokogiri-class.rb
Extend Nokogiri::XML::Element to have add_class(), remove_class(), has_class()
to help the method chain.

# rotator.rb
"Rotator" generator class which the caller takes "/", "-", "\", "|" to show
an ASCII rotator at STDERR.

## Authors and acknowledgment
suzuki toshiya, 2012-2024

## License
GNU GPL version 3
