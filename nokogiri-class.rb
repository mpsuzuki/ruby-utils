require "zlib"

class Nokogiri::XML::Element
  def add_class(cls)
    if (self["class"] == nil)
      self["class"] = [cls].flatten.join(" ")
    else
      self["class"] = (self["class"].split(/\s+/) | [cls]).flatten.join(" ")
    end
    return self
  end

  def remove_class(cls)
    if (self["class"] != nil)
      self["class"] = (self["class"].split(/\s+/) - [cls].flatten).join(" ")
    end
    return self
  end

  def has_class(cls)
    if (self["class"] == nil)
      return false
    else
      return self["class"].split(/\s+/).any?{|c| c.downcase == cls.downcase}
    end
  end

  def get_classes()
    if (self["class"])
      return self["class"].split(/\s+/)
    else
      return []
    end
  end
end

class Nokogiri::XML::Document
  def self.read_maybe_gz(path)
    rawF = File::open(path, "r")
    f = rawF
    if (["gz", "svgz"].include?(path.split(".").last.downcase()))
      f = Zlib::GzipReader.new(rawF)
    end
    doc = Nokogiri::XML::Document.parse(f.read())
    f.close()
    return doc
  end

  def write_maybe_gz(path)
    rawF = File::open(path, "wb")
    f = rawF
    if (["gz", "svgz"].include?(path.split(".").last.downcase()))
      f = Zlib::GzipWriter.new(rawF, level = 9)
    end
    f.puts(self.to_s())
    f.close()
  end
end
